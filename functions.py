import decimal
import numpy


def postcodeGenerator(first_code, second_code):
    postcodes = []
    first_code = first_code.split("-")
    second_code = second_code.split("-")

    for code_part1 in range(int(first_code[0]), int(second_code[0])+1, 1):
        for code_part2 in range(1, 1000, 1):
            if code_part1 == int(first_code[0]) and code_part2 < int(first_code[1]):
                continue
            if code_part1 == int(second_code[0]) and code_part2 > int(second_code[1]):
                break
            zero = ''
            if code_part2 < 10:
                zero = "00"
            elif code_part2 < 100:
                zero = '0'
            postcodes.append(str(code_part1) + '-' + zero + str(code_part2))
    return postcodes


def missingElements(items):
    items = list(set(items))
    values = []
    for value in range(1,items[-1]):
        if value not in items:
            values.append(value)
    return values


def decimalValuesGenerator(start_value, stop_value):
    values = []
    for value in numpy.arange(start_value, stop_value+0.5, 0.5):
        values.append(decimal.Decimal(value))
    return values